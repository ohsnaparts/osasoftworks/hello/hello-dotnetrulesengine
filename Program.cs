﻿using RulesEngine.Models;

// build up a workflow consisting of n rules
var workflow = new Workflow
{
    WorkflowName = "EmployeeCount Workflow",
    Rules = new Rule[]
    {
        new ()
        {
            RuleName = "EmployeeCount Warning Threshold Rule",
            SuccessEvent = "EmployeeCount within warning tolerance",
            ErrorMessage = "EmployeeCount exceeded the configured limit.",
            Expression = "presence.employeeCount > 10",
            RuleExpressionType = RuleExpressionType.LambdaExpression
        },
        new ()
        {
            RuleName = "EmployeeCount Critical Threshold Rule",
            SuccessEvent = "EmployeeCount reached critical tolerance",
            ErrorMessage = "EmployeeCount within tolerant limits",
            Expression = "presence.employeeCount > 20",
            RuleExpressionType = RuleExpressionType.LambdaExpression
        }
    }
};

// register workflow with rules engine
var rulesEngine = new RulesEngine.RulesEngine(new[] {workflow});


while (true)
{
    // read input data
    Console.Write("Employee count: ");
    var input = Console.ReadLine();
    
    if (string.IsNullOrWhiteSpace(input))
    {
        break;
    }
    if (!byte.TryParse(input, out var employeeCount))
    {
        continue;
    }

    // use complex datatype to see if RulesEngine expressions can handle them
    var presence = new Presence(employeeCount);
    
    // execute all rules in the specified workflow
    var ruleExecutionResults = await rulesEngine.ExecuteAllRulesAsync(
        workflow.WorkflowName,
        new RuleParameter("presence", presence)
    );
    
    // print only passing rules
    foreach (var ruleExecutionResult in ruleExecutionResults.Where(r => r.IsSuccess))
    {
        Console.Error.WriteLine("Triggered rule {0}!", ruleExecutionResult.Rule.RuleName);
    }
}